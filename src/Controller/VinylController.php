<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use function Symfony\Component\String\u;

class VinylController extends AbstractController
{
    #[Route('/')]
    public function homePage(): Response
    {
        return $this->render('vinyl/homePage.html.twig', [
            'title' => 'TODO',
            'tracks' => [
                ['song' => 'proba', 'artist' => 'alabala'],
                ['song' => 'proba1', 'artist' => 'alabala1'],
                ['song' => 'proba2', 'artist' => 'alabala2'],
            ]
        ]);
    }

    #[Route('/browse/{slug}')]
    public function browse(?string $slug = null): Response
    {
        $title = $slug ? 'Genre: ' . u(str_replace('-', ' ', $slug))->title(true) : 'All Genres';

        return new Response($title);
    }
}